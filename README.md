# COMPANY website developer challenge

## Wireframes

- [Small](/wireframes/small.png) (320x480 / iphone SE)
- [Medium](/wireframes/medium.png) (768x1024 / iPad portrait)
- [Large](/wireframes/large.png) (1024x768 / iPad landscape)

## Limitations

- Use vanilla HTML, CSS, and JavaScript
    - No libraries, frameworks, components, etc

## Browser support

- IE9+

Don't know how to test IE in MacOS or GNU/Linux? See [modern.ie](https://modern.ie).

## Specifications

### Meta data

| Meta | Contents |
| :-- | :-- |
| title | COMPANY website developer challenge |

### Fixed navbar

Containing:

1. Menu Toggle
2. Menu
   1. Link to Section 1
   2. Link to Section 2

The toggle button should toggle a menu containing the two links on small screens.

### Section 1

#### Image

Place an image to the right of content below on large screens.

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

#### Button

`Lorem ipsum dolor sit amet`

### Section 2

Place two components made up of the following content side-by-side on large screens and stacked on small screens.

#### Image

Place an image to the left of the content below on tablets.

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

## Submission

1. Host your submission using Gitlab pages
    - Don't mention anything identifying in your repository (to avoid copycats)
2. Send the URL to your submission to COMPANY

## Bonus

The challenge above was designed to be completable within one remote working day. If you ( were able to / think you can ) complete it in less than one remote working day, consider taking on one or more of the following bonus challenges:

- Adjust the height and spacing of the first section so that the second section is always a little visible - attracting the user to scroll
- Make it "no-js first"
- Make it accessible
- Translate it into another language
    - Preferably a right-to-left language
    - Don't worry about translation accuracy
        - Any similar placeholder content will do

**Important**:

1. The limitations above apply to the bonus challenges too
2. Please state which bonus challenges you have attempted; if any
3. A submission with no bonuses completed is better than a submission with incomplete or poorly implemented bonuses
