// Utils
function containsClass(element, className) {
  return (" " + element.className + " ").indexOf(" " + className + " ") > -1;
};
function toggleClass(element, className) {
  return (containsClass(element, className) ? removeClass : addClass)(element, className);
};
function addClass(element, className) {
  return containsClass(element, className) ? false : (element.className += " " + className);
};
function removeClass(element, className) {
  return element.className = (" " + element.className + " ").replace(" " + className + " ", " ");
};

// DOM loaded here
(function () {

  var doc = document,
      body = doc.querySelector('body');

  // IF JS (read more in the index.css file)
  body.setAttribute('class', '');

  // Menu toggle
  var menuBar = doc.querySelector('.menu'),
      menuList = doc.querySelector('.menu__list'),
      menuToggleLink = doc.querySelectorAll('.menu__list__item'),
      menuToggleDOM =
      '<span id="menuToggle" class="menu__toggle" tabindex="0" role="button">'+
        '<svg id="Layer_1" class="menu__toggle__icon" version="1.1" viewBox="0 0 32 32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">'+
          '<path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/>'+
        '</svg>'+
      '</span>',
      menuToggleWrapper = document.createElement("span");

  // add it to the navBar
  menuToggleWrapper.innerHTML = menuToggleDOM;
  menuBar.insertBefore(menuToggleWrapper, menuList);
  // make it work on click
  var menuToggle = doc.getElementById('menuToggle');
  menuToggle.addEventListener('click', function(){
    toggleClass(body, "js-menu-opened");
  })
  // and in accessible way (tab and ENTER/SPACE)
  menuToggle.addEventListener('keydown', function(ev) {
    if(ev.keyCode && ev.keyCode == 13 || ev.keyCode && ev.keyCode == 32) {
      toggleClass(body, "js-menu-opened");
    }
  });


}());
